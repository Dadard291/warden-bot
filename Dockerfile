FROM python:latest

ARG BOT_TOKEN

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

USER root
RUN chmod a+w ./logs

ENV BOT_TOKEN=$BOT_TOKEN

CMD ["python", "WardenBot/warden_bot.py"]
