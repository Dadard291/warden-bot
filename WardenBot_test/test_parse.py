import unittest

from WardenBot.parsing.main_parse import main_parse


class parsing_test(unittest.TestCase):
    def test_main_parse(self):
        inp = "salut"
        resp = main_parse(inp)
        assert resp == inp
